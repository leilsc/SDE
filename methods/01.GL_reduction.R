#=================================================================================================
# Bin NDVI data 
#=================================================================================================

library(raster)
# library(radiomics)
# The radiomics package was removed from CRAN, please run the function we used from
# the radiomics package in the R Script "radiomics_fun.R"

#=================================================================================================
# Data: NDVI.NAfill.2018.nee.tif 
# NDVI data from the MODIS sensor
# averaged over the growing season 2018
# NA gaps closed with local neighborhood average
# downloaded from goodle earth engine
# study region in north eastern Eurasia (NEE).
#=================================================================================================

nee <- raster("data/NDVI.NAfill.2018.nee.tif")
neemat <- matrix(nee, nrow=nrow(nee), ncol =ncol(nee), byrow = TRUE)

nee1000 <- discretizeImage(neemat, n_grey = 1000)
nee1000 <- raster(nee1000, crs = crs(nee))
extent(nee1000) <- extent(nee)
writeRaster(nee1000, "data/ndvi1000GL.tif")

nee500 <- discretizeImage(neemat, n_grey = 500)
nee500 <- raster(nee500, crs = crs(nee))
extent(nee500) <- extent(nee)
writeRaster(nee500, "data/ndvi500GL.tif")

nee100 <- discretizeImage(neemat, n_grey = 100)
nee100 <- raster(nee100, crs = crs(nee))
extent(nee100) <- extent(nee)
writeRaster(nee100, "data/ndvi100GL.tif")

nee50 <- discretizeImage(neemat, n_grey = 50)
nee50 <- raster(nee50, crs = crs(nee))
extent(nee50) <- extent(nee)
writeRaster(nee50, "data/ndvi50GL.tif")

nee25 <- discretizeImage(neemat, n_grey = 25)
nee25 <- raster(nee25, crs = crs(nee))
extent(nee25) <- extent(nee)
writeRaster(nee25, "data/ndvi25GL.tif")

nee15 <- discretizeImage(neemat, n_grey = 15)
nee15 <- raster(nee15, crs = crs(nee))
extent(nee15) <- extent(nee)
writeRaster(nee15, "data/ndvi15GL.tif")

nee10 <- discretizeImage(neemat, n_grey = 10)
nee10 <- raster(nee10, crs = crs(nee))
extent(nee10) <- extent(nee)
writeRaster(nee10, "data/ndvi10GL.tif")

nee9 <- discretizeImage(neemat, n_grey = 9)
nee9 <- raster(nee9, crs = crs(nee))
extent(nee9) <- extent(nee)
writeRaster(nee9, "data/ndvi9GL.tif")

nee8 <- discretizeImage(neemat, n_grey = 8)
nee8 <- raster(nee8, crs = crs(nee))
extent(nee8) <- extent(nee)
writeRaster(nee8, "data/ndvi8GL.tif")

nee7 <- discretizeImage(neemat, n_grey = 7)
nee7 <- raster(nee7, crs = crs(nee))
extent(nee7) <- extent(nee)
writeRaster(nee7, "data/ndvi7GL.tif")

nee6 <- discretizeImage(neemat, n_grey = 6)
nee6 <- raster(nee6, crs = crs(nee))
extent(nee6) <- extent(nee)
writeRaster(nee6, "data/ndvi6GL.tif")

nee5 <- discretizeImage(neemat, n_grey = 5)
nee5 <- raster(nee5, crs = crs(nee))
extent(nee5) <- extent(nee)
writeRaster(nee5, "data/ndvi5GL.tif")

nee4 <- discretizeImage(neemat, n_grey = 4)
nee4 <- raster(nee4, crs = crs(nee))
extent(nee4) <- extent(nee)
writeRaster(nee4, "data/ndvi4GL.tif")

nee3 <- discretizeImage(neemat, n_grey = 3)
nee3 <- raster(nee3, crs = crs(nee))
extent(nee3) <- extent(nee)
writeRaster(nee3, "data/ndvi3GL.tif")

nee2 <- discretizeImage(neemat, n_grey = 2)
nee2 <- raster(nee2, crs = crs(nee))
extent(nee2) <- extent(nee)
writeRaster(nee2, "data/ndvi2GL.tif")

#=================================================================================================