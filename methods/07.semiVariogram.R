#=================================================================================================
# Semi-variogram estimation of NDVI data, large and small dominant features, 
# smallest and largest typical structural diversity features 
#=================================================================================================
# If you ran 'GL_reduction.R', you can load NDVI reduced to 50 gray levels instead of NDVI
# If you ran 'scaleSpaceMultiresDecomp.R' on NDVI data reduced to 50 GL, you can load
# the respective small and large dominant features instead
#=================================================================================================

library(gstat)
library(raster)
library(spam)

#=================================================================================================
# Model setting for semi-variogram estimation and function to retrieve effective range
#=================================================================================================

mV <- vgm(model = c("Mat"))

# effective range all models - nugget 0
get_effrange <- function(covmat_y, h) {
  imin <- which.min(abs(covmat_y - .05*covmat_y[1])^2)
  return(h[imin])
}
h <- 1:200

#=================================================================================================
# NDVI data
#=================================================================================================

nee <- raster("data/NDVI.NAfill.2018.nee.tif")
spdf.nee<- rasterToPoints(nee , fun=NULL, spatial = TRUE)

ev.nee <- variogram(NDVI.NAfill.2018.nee   ~ coordinates(spdf.nee), 
                           data = spdf.nee, cressi =TRUE, covariogram  =FALSE, verbose=TRUE)

vm.nee <- fit.variogram(ev.nee, model = mV, fit.method = 7, fit.kappa =TRUE)
# plot(ev.nee, model = vm.nee)

param.nee <- c(vm.nee$range, vm.nee$psill, vm.nee$kappa, 0)
effrange.nee <- get_effrange(cov.mat(h, param.nee), h)

# save data
nee.dat <- cbind("NDVI.nee", vm.nee, effrange.nee)
colnames(nee.dat)[1] <- c("model name")
colnames(nee.dat)[11] <- c("effective range")

#=================================================================================================
# Large dominant features
#=================================================================================================

LD.nee <- raster("results/scaleSpace/tif/largeDom.tif")
spdf.LD.nee<- rasterToPoints(LD.nee , fun=NULL, spatial = TRUE)

ev.LD.nee <- variogram(largeDom   ~ coordinates(spdf.LD.nee), 
                    data = spdf.LD.nee, cressi =TRUE, covariogram  =FALSE, verbose=TRUE)

vm.LD.nee <- fit.variogram(ev.LD.nee, model = mV, fit.method = 7, fit.kappa =TRUE)

param.LD.nee <- c(vm.LD.nee$range, vm.LD.nee$psill, vm.LD.nee$kappa, 0)
effrange.LD.nee <- get_effrange(cov.mat(h, param.LD.nee), h)

# save data
LD.nee.dat <- cbind("LD.nee", vm.LD.nee, effrange.LD.nee)
colnames(LD.nee.dat)[1] <- c("model name")
colnames(LD.nee.dat)[11] <- c("effective range")

#=================================================================================================
# Small dominant features
#=================================================================================================

SD.nee <- raster("results/scaleSpace/tif/smallDom.tif")
spdf.SD.nee<- rasterToPoints(SD.nee , fun=NULL, spatial = TRUE)

ev.SD.nee <- variogram(smallDom ~ coordinates(spdf.SD.nee), 
                       data = spdf.SD.nee, cressi =TRUE, covariogram  =FALSE, verbose=TRUE)

vm.SD.nee <- fit.variogram(ev.SD.nee, model = mV, fit.method = 7, fit.kappa =TRUE)

param.SD.nee <- c(vm.SD.nee$range, vm.SD.nee$psill, vm.SD.nee$kappa, 0)
effrange.SD.nee <- get_effrange(cov.mat(h, param.SD.nee), h)

# save data
SD.nee.dat <- cbind("SD.nee", vm.SD.nee, effrange.SD.nee)
colnames(SD.nee.dat)[1] <- c("model name")
colnames(SD.nee.dat)[11] <- c("effective range")

#=================================================================================================
# Diversity maps. Smallest and largest typical features, within ideal GL range
#=================================================================================================
# Borders
#=================================================================================================

border.max <- raster("results/strucDiv/tif/vi_entd2_wsl3.tif")
border.max <- log(border.max)
spdf.border.max<- rasterToPoints(border.max , fun=NULL, spatial = TRUE)
fin.border.max <- spdf.border.max[is.finite(spdf.border.max$layer),]

ev.border.max <- variogram(layer  ~ coordinates(fin.border.max), 
                   data = fin.border.max, cutoff = 50, width=1, 
                   cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.border.max <- fit.variogram(ev.border.max, model = mV, fit.method = 7, fit.kappa =TRUE)
param.border.max <- c(vm.border.max$range, vm.border.max$psill, vm.border.max$kappa, 0)
effrange.border.max <- get_effrange(cov.mat(h, param.border.max), h)

# save data
border.max.dat <- cbind("BorderMax", vm.border.max, effrange.border.max)
colnames(border.max.dat)[1] <- c("model name")
colnames(border.max.dat)[11] <- c("effective range")

#=================================================================================================
# Line Features
#=================================================================================================

line.min <- raster("results/strucDiv/tif/vi10_entd0_wsl5.tif")
line.min <- log(line.min)
spdf.line.min<- rasterToPoints(line.min , fun=NULL, spatial = TRUE)
fin.line.min <- spdf.line.min[is.finite(spdf.line.min$layer),]

ev.line.min <- variogram(layer  ~ coordinates(fin.line.min), 
                           data = fin.line.min, cutoff = 50, width=1, 
                           cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.line.min <- fit.variogram(ev.line.min, model = mV, fit.method = 7, fit.kappa =TRUE)
param.line.min <- c(vm.line.min$range, vm.line.min$psill, vm.line.min$kappa, 0)
effrange.line.min <- get_effrange(cov.mat(h, param.line.min), h)

# save data
line.min.dat <- cbind("LineMin", vm.line.min, effrange.line.min)
colnames(line.min.dat)[1] <- c("model name")
colnames(line.min.dat)[11] <- c("effective range")

#=================================================================================================

line.max <- raster("results/strucDiv/tif/vi10_entd0_wsl11.tif")
line.max <- log(line.max)
spdf.line.max<- rasterToPoints(line.max , fun=NULL, spatial = TRUE)
fin.line.max <- spdf.line.max[is.finite(spdf.line.max$layer),]

ev.line.max <- variogram(layer  ~ coordinates(fin.line.max), 
                         data = fin.line.max, cutoff = 50, width=1, 
                         cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.line.max <- fit.variogram(ev.line.max, model = mV, fit.method = 7, fit.kappa =TRUE)
param.line.max <- c(vm.line.max$range, vm.line.max$psill, vm.line.max$kappa, 0)
effrange.line.max <- get_effrange(cov.mat(h, param.line.max), h)

# save data
line.max.dat <- cbind("LineMax", vm.line.max, effrange.line.max)
colnames(line.max.dat)[1] <- c("model name")
colnames(line.max.dat)[11] <- c("effective range")

#=================================================================================================
# Patches, delta = 1
#=================================================================================================

patch.min <- raster("results/strucDiv/tif/vi_entd1_wsl7.tif")
patch.min <- log(patch.min)
spdf.patch.min<- rasterToPoints(patch.min , fun=NULL, spatial = TRUE)
fin.patch.min <- spdf.patch.min[is.finite(spdf.patch.min$layer),]

ev.patch.min <- variogram(layer  ~ coordinates(fin.patch.min), 
                         data = fin.patch.min, cutoff = 50, width=1, 
                         cressi =TRUE, covariogram  =FALSE, verbose=TRUE)

vm.patch.min <- fit.variogram(ev.patch.min, model = mV, fit.method = 7, fit.kappa =TRUE)
param.patch.min <- c(vm.patch.min$range, vm.patch.min$psill, vm.patch.min$kappa, 0)
effrange.patch.min <- get_effrange(cov.mat(h, param.patch.min), h)

# save data
patch.min.dat <- cbind("PatchMin", vm.patch.min, effrange.patch.min)
colnames(patch.min.dat)[1] <- c("model name")
colnames(patch.min.dat)[11] <- c("effective range")

#=================================================================================================

patch.max <- raster("results/strucDiv/tif/vi_entd1_wsl19.tif")
patch.max <- log(patch.max)
spdf.patch.max<- rasterToPoints(patch.max , fun=NULL, spatial = TRUE)
fin.patch.max <- spdf.patch.max[is.finite(spdf.patch.max$layer),]

ev.patch.max <- variogram(layer  ~ coordinates(fin.patch.max), 
                          data = fin.patch.max, cutoff = 50, width=1, 
                          cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.patch.max <- fit.variogram(ev.patch.max, model = mV, fit.method = 7, fit.kappa =TRUE)
param.patch.max <- c(vm.patch.max$range, vm.patch.max$psill, vm.patch.max$kappa, 0)
effrange.patch.max <- get_effrange(cov.mat(h, param.patch.max), h)

# save data
patch.max.dat <- cbind("PatchMax", vm.patch.max, effrange.patch.max)
colnames(patch.max.dat)[1] <- c("model name")
colnames(patch.max.dat)[11] <- c("effective range")

#=================================================================================================
# Patches/ Hotspots, delta = 2
#=================================================================================================

hotspot.min <- raster("results/strucDiv/tif/vi_entd2_wsl7.tif")
hotspot.min <- log(hotspot.min)
spdf.hotspot.min<- rasterToPoints(hotspot.min , fun=NULL, spatial = TRUE)
fin.hotspot.min <- spdf.hotspot.min[is.finite(spdf.hotspot.min$layer),]

ev.hotspot.min <- variogram(layer  ~ coordinates(fin.hotspot.min), 
                          data = fin.hotspot.min, cutoff = 50, width=1, 
                          cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.hotspot.min <- fit.variogram(ev.hotspot.min, model = mV, fit.method = 7, fit.kappa =TRUE)
param.hotspot.min <- c(vm.hotspot.min$range, vm.hotspot.min$psill, vm.hotspot.min$kappa, 0)
effrange.hotspot.min <- get_effrange(cov.mat(h, param.hotspot.min), h)

# save data
hotspot.min.dat <- cbind("HotspotMin", vm.hotspot.min, effrange.hotspot.min)
colnames(hotspot.min.dat)[1] <- c("model name")
colnames(hotspot.min.dat)[11] <- c("effective range")

#=================================================================================================

hotspot.max <- raster("results/strucDiv/tif/vi_entd2_wsl19.tif")
hotspot.max <- log(hotspot.max)
spdf.hotspot.max<- rasterToPoints(hotspot.max , fun=NULL, spatial = TRUE)
fin.hotspot.max <- spdf.hotspot.max[is.finite(spdf.hotspot.max$layer),]

ev.hotspot.max <- variogram(layer  ~ coordinates(fin.hotspot.max), 
                            data = fin.hotspot.max, cutoff = 50, width=1, 
                            cressi =TRUE, covariogram  =FALSE, verbose=TRUE)
vm.hotspot.max <- fit.variogram(ev.hotspot.max, model = mV, fit.method = 7, fit.kappa =TRUE)
param.hotspot.max <- c(vm.hotspot.max$range, vm.hotspot.max$psill, vm.hotspot.max$kappa, 0)
effrange.hotspot.max <- get_effrange(cov.mat(h, param.hotspot.max), h)

# save data
hotspot.max.dat <- cbind("HotspotMax", vm.hotspot.max, effrange.hotspot.max)
colnames(hotspot.max.dat)[1] <- c("model name")
colnames(hotspot.max.dat)[11] <- c("effective range")

#=================================================================================================
# Combine and save data
#=================================================================================================

SemiVarDivMaps <- rbind(nee.dat, LD.nee.dat, SD.nee.dat, border.min.dat, border.max.dat, line.min.dat, line.max.dat, 
                      patch.min.dat, patch.max.dat, hotspot.min.dat, hotspot.min.dat)

dir.create("results/semiVar")
write.table(SemiVarDivMaps, "results/semiVar/semiVar.csv", col.names=TRUE, row.names=FALSE, sep = ",")

#=================================================================================================
