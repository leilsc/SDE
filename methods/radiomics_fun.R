#==============================================================================================
# Function to reduce bin data into equal size bins
# The radiomics package was removed from CRAN, we used this function the radiomics package.
#==============================================================================================

discretizeImage <- function(data, n_grey=32, verbose=TRUE){
  #Error checking
  #Check validity of input
  if (!is.matrix(data)) {
    stop(paste0("Object of class ", class(data), ".  is.matrix(object) must evaluate TRUE."))
  }
  if (any(data < 0, na.rm=TRUE)) {
    stop("Object contains negative values. All values must be greater than 0.")
  }
  
  #Not a perfect solution. Makes n_grey breaks, but doesn't necessarily populate all of them
  # eg. n_gey could be 100, but only 75 of the levels are used by pixels
  l_unique <- length(unique(c(data)))
  
  #Make sure discretization is valid
  if(l_unique == 0 ) stop("Function not valid for empty input")
  if(sum(is.na(data)) == length(data)){
    if(verbose) warning("Matrix composed entirely of NA's\nReturning Matrix")
    return(data)
  } 
  
  #If we don't need to do anything, we don't do anything
  if(n_grey == l_unique){
    return(data)
  } else if(n_grey > l_unique){
    if(verbose) message(sprintf("n_grey (%d) cannot be larger than the number of gray levels in the image (%d)", n_grey, l_unique))
    if(verbose) message(sprintf("n_grey set to %d", l_unique))
    return(data)
  } 
  
  discretized <- cut(data, breaks=seq(min(data, na.rm=TRUE), max(data, na.rm=TRUE), length.out=(n_grey + 1)),
                     labels = seq(1, n_grey, 1),
                     include.lowest=TRUE, right=FALSE) 
  return(matrix(as.numeric(discretized), nrow=nrow(data)))
}

#==============================================================================================