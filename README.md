# SDE

This Git repository belongs to the manuscript "Structural diversity entropy: a unified diversity measure to detect latent landscape features". It contains the tarball for the R Package StrucDiv, data and R scripts for reproducibility of results.
